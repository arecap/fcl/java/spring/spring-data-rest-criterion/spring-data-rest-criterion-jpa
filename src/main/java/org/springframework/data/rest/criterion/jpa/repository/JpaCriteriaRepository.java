/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.data.rest.criterion.jpa.repository;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaContext;
import org.springframework.data.rest.criterion.jpa.query.JpaQueryCriteriaUtil;
import org.springframework.data.rest.criterion.query.QueryCriteria;
import org.springframework.data.rest.criterion.query.QueryCriteriaExecutor;
import org.springframework.data.rest.criterion.repository.CriteriaRepositorySupport;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public class JpaCriteriaRepository  implements CriteriaRepositorySupport, QueryCriteriaExecutor {

    private final Log logger = LogFactory.getLog(JpaCriteriaRepository.class);

    @Autowired
    private JpaContext jpaContext;

    private EntityManager getEntityManager(Class<?> entityType) {
        return Optional.ofNullable(entityType).map(jpaContext::getEntityManagerByManagedType).orElse(null);
    }


    private Criteria getCriteria(QueryCriteria queryCriteria) {
        return Optional.ofNullable(JpaQueryCriteriaUtil.getEntityDefinition(queryCriteria.getEntityName())).
                map( entityDefinition -> getCriteria(queryCriteria, entityDefinition)).
                orElse(null);
    }

    private Criteria getCriteria(QueryCriteria queryCriteria, BeanDefinition entityDefinition) {
        Criteria criteria = getExecutableCriteria(entityDefinition.getBeanClassName(), JpaQueryCriteriaUtil.
                createDetachedCriteria(entityDefinition, queryCriteria.getAssociatedPaths())).
        add(JpaQueryCriteriaUtil.
                buildCriterion(getExecutableQueryCriteria(queryCriteria)));
        ProjectionList projectionList = JpaQueryCriteriaUtil.createProjectionList(queryCriteria);
        return projectionList.getLength() == 0 ? criteria : criteria.setProjection(projectionList);
    }

    private Criteria getExecutableCriteria(String entityName, DetachedCriteria detachedCriteria) {
        try {
            return detachedCriteria.
                    getExecutableCriteria(getEntityManager(Class.forName(entityName)).unwrap(Session.class));
        } catch (ClassNotFoundException e) {
        }
        return null;
    }

    private void addOrder(Criteria criteria, Sort sort) {
        sort.iterator().forEachRemaining( order -> {
            String[] propertyDirection = order.getProperty().split("\\.");
            if(propertyDirection.length == 1 || propertyDirection[1].equalsIgnoreCase("asc")) {
                criteria.addOrder(Order.asc(propertyDirection[0]));
            } else {
                criteria.addOrder(Order.desc(propertyDirection[0]));
            }
        });
    }

    public Page<?> findAll(QueryCriteria queryCriteria, Pageable pageable) {
        Criteria criteria = getCriteria(queryCriteria).setFirstResult(pageable.getPageNumber()).
                setMaxResults(pageable.getPageSize()).
                setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        if(pageable.getSort() != null) {
            addOrder(criteria, pageable.getSort());
        }
        return new PageImpl<Object>(criteria.list(), pageable,
                count(queryCriteria));
    }

    public long count(QueryCriteria queryCriteria) {
        return (long) getCriteria(queryCriteria).setProjection(Projections.projectionList().add(Projections.rowCount())).
                setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).uniqueResult();
    }

    @Override
    public List getQueryCriteriaValues(QueryCriteria queryCriteria) {
        return getCriteria(queryCriteria).list();
    }

}

/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.data.rest.criterion.jpa.boot;


import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.data.rest.criterion.jpa.annotation.EnableJpaCriteriaRepository;
import org.springframework.data.rest.criterion.jpa.controller.CriteriaRepositoryRestController;
import org.springframework.data.rest.criterion.jpa.repository.JpaCriteriaRepository;

import javax.persistence.Entity;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
@Configurable
public class JpaCriteriaRepositoryBoot  {


    public static Set<String> packagesToScans = new LinkedHashSet<>();

    public static Set<BeanDefinition> findEntityDefinitions() {
        Set<BeanDefinition> entityDefinitions = new LinkedHashSet<>();
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(Entity.class));
        for(String packageToScan: packagesToScans) {
            entityDefinitions.addAll(scanner.findCandidateComponents(packageToScan));
        }
        return entityDefinitions;
    }


    @Bean
    public static JpaCriteriaRepository getJpaCriteriaRepository() {
        return new JpaCriteriaRepository();
    }

    @Bean
    public static CriteriaRepositoryRestController getCriteriaRepositoryRestController() {
        return new CriteriaRepositoryRestController();
    }


    public static class Registrar implements ImportBeanDefinitionRegistrar {

        @Override
        public void registerBeanDefinitions(AnnotationMetadata annotationMetadata, BeanDefinitionRegistry beanDefinitionRegistry) {
            AnnotationAttributes attributes = AnnotationAttributes.fromMap(
                    annotationMetadata.getAnnotationAttributes(EnableJpaCriteriaRepository.class.getName()));
            String[] basePackages = attributes.getStringArray("basePackages");
            packagesToScans.addAll(Arrays.asList(basePackages));

        }

    }

}

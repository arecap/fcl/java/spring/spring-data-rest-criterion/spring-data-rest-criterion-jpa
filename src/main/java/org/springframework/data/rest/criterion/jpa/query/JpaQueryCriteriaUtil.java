/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.data.rest.criterion.jpa.query;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.criterion.*;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.data.rest.criterion.jpa.boot.JpaCriteriaRepositoryBoot;
import org.springframework.data.rest.criterion.query.ProjectionQuery;
import org.springframework.data.rest.criterion.query.QueryCriteria;
import org.springframework.data.rest.criterion.query.QueryCriteriaUtil;

import javax.persistence.Entity;
import java.util.Optional;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public final class JpaQueryCriteriaUtil {

    private final static Log logger = LogFactory.getLog(JpaQueryCriteriaUtil.class);

    public static BeanDefinition getEntityDefinition(String entityName) {
        for (BeanDefinition entityDefinition : JpaCriteriaRepositoryBoot.findEntityDefinitions()) {
            try {
                Class<?>  entityClass = Class.forName(entityDefinition.getBeanClassName());
                Entity entity = AnnotationUtils.findAnnotation(entityClass, Entity.class);
                if (entity.name().equalsIgnoreCase(entityName)) {
                    return entityDefinition;
                }
            } catch (ClassNotFoundException e) {
                logger.warn("Entity class [" + entityDefinition.getBeanClassName() + "] not found exception!", e);
            }
        }
        logger.warn("No @Entity name [" + entityName
                + "] bean class defined in @EnableJpaCriteriaRepository basePackages!");
        return null;
    }

    public static DetachedCriteria createDetachedCriteria(QueryCriteria queryCriteria) {
        return createDetachedCriteria(getEntityDefinition(queryCriteria.getEntityName()),
                queryCriteria.getAssociatedPaths());
    }

    public static DetachedCriteria createDetachedCriteria(BeanDefinition entityDefinition, String[] associatedPaths) {
        try {
            DetachedCriteria detachedCriteria = DetachedCriteria
                    .forClass(Class.forName(entityDefinition.getBeanClassName()));
            return Optional.ofNullable(associatedPaths)
                    .map((associated) -> createAlliases(detachedCriteria, associated))
                    .orElse(detachedCriteria);

        } catch (ClassNotFoundException e) {
            logger.warn("Entity class [" + entityDefinition.getBeanClassName() + "] not found exception!", e);
        }
        return null;
    }

    public static DetachedCriteria createAlliases(DetachedCriteria detachedCriteria, String... associatedPaths) {
        for(String associatedPath: associatedPaths) {
            detachedCriteria.createAlias(associatedPath, associatedPath.toLowerCase());
        }
        return detachedCriteria;
    }

    public static Criterion buildCriterion(QueryCriteria queryCriteria) {
       return QueryCriteriaUtil.isInclusionCriterion(queryCriteria) ?
               JpaQueryCriterionResolver.inclusion.resolve(queryCriteria) :
               (QueryCriteriaUtil.isJunctionCriterion(queryCriteria) ? JpaQueryCriterionResolver.junction.resolve(queryCriteria) :
                       JpaQueryCriterionResolver.property.resolve(queryCriteria));
    }


    public static ProjectionList createProjectionList(QueryCriteria queryCriteria) {
        ProjectionList projectionList = Projections.projectionList();
        if(queryCriteria.getProjections() != null && queryCriteria.getProjections().size() > 0) {
            queryCriteria.getProjections().stream().forEach(
                    projectionQuery -> {
                        Projection projection = createProjection(projectionQuery);
                        if(projection != null) {
                            projectionList.add(projection);
                        }
                    }
            );
        }
        return projectionList;
    }

    public static Projection createProjection(ProjectionQuery projectionQuery) {
        return new JpaProjectionResolver().resolve(projectionQuery);
    }

}

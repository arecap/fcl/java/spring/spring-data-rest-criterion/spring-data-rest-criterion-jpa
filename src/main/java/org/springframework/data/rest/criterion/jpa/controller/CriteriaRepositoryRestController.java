/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.data.rest.criterion.jpa.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.criterion.query.QueryCriteria;
import org.springframework.data.rest.criterion.repository.CriteriaRepositorySupport;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
@RestController
public class CriteriaRepositoryRestController {

    @Autowired
    private CriteriaRepositorySupport<?> criteriaRepositorySupport;

    @RequestMapping(value = "/criteria/findAll", method = RequestMethod.POST)
    public Page<?> findAll(@RequestBody QueryCriteria queryCriteria, Pageable pageable) {
        return criteriaRepositorySupport.findAll(queryCriteria, pageable);
    }

    @RequestMapping(value = "/criteria/count", method = RequestMethod.POST)
    public long count(@RequestBody QueryCriteria queryCriteria) {
        return criteriaRepositorySupport.count(queryCriteria);
    }

    @RequestMapping(value = "/criteria/valid", method = RequestMethod.POST)
    public boolean valid(@RequestBody QueryCriteria queryCriteria) {
        return criteriaRepositorySupport.valid(queryCriteria);
    }

}

/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.data.rest.criterion.jpa.query;


import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.rest.criterion.query.CriterionDefinition;
import org.springframework.data.rest.criterion.query.QueryCriteria;
import org.springframework.data.rest.criterion.query.QueryCriterionResolver;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public enum JpaQueryCriterionResolver implements QueryCriterionResolver<Criterion> {

    property {
        @Override
        public Criterion resolve(QueryCriteria queryCriteria) {
            switch (queryCriteria.getCriterion()) {
                case idEq:
                    return Restrictions.idEq(queryCriteria.getValues().get(0));
                case eq:
                    return Restrictions.eq(queryCriteria.getPropertyName(), queryCriteria.getValues().get(0));
                case ne:
                    return Restrictions.ne(queryCriteria.getPropertyName(), queryCriteria.getValues().get(0));
                case like:
                    return Restrictions.like(queryCriteria.getPropertyName(), queryCriteria.getValues().get(0));
                case ilike:
                    return Restrictions.ilike(queryCriteria.getPropertyName(), queryCriteria.getValues().get(0));
                case gt:
                    return Restrictions.gt(queryCriteria.getPropertyName(), queryCriteria.getValues().get(0));
                case lt:
                    return Restrictions.lt(queryCriteria.getPropertyName(), queryCriteria.getValues().get(0));
                case le:
                    return Restrictions.le(queryCriteria.getPropertyName(), queryCriteria.getValues().get(0));
                case ge:
                    return Restrictions.ge(queryCriteria.getPropertyName(), queryCriteria.getValues().get(0));
                case between:
                    return Restrictions.between(queryCriteria.getPropertyName(), queryCriteria.getValues().get(0),
                            queryCriteria.getValues().get(1));
            }
            return null;
        }
    },

    junction {
        @Override
        public Criterion resolve(QueryCriteria queryCriteria) {
            Set<Criterion> junctionCriterion = new LinkedHashSet<>();
            for(QueryCriteria junctionQuery: queryCriteria.getJunctionQueries()) {
                junctionCriterion.add(JpaQueryCriteriaUtil.buildCriterion(queryCriteria));
            }
            return queryCriteria.getCriterion().equals(CriterionDefinition.conjunction) ?
                    Restrictions.and(junctionCriterion.toArray(new Criterion[junctionCriterion.size()])) :
                    Restrictions.or(junctionCriterion.toArray(new Criterion[junctionCriterion.size()]));
        }

    },

    inclusion {
        @Override
        public Criterion resolve(QueryCriteria queryCriteria) {
            return Restrictions.in(queryCriteria.getPropertyName(), queryCriteria.getValues());
        }

    }

}

/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.data.rest.criterion.jpa.query;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.springframework.data.rest.criterion.query.ProjectionQuery;
import org.springframework.data.rest.criterion.query.ProjectionResolver;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public final class JpaProjectionResolver implements ProjectionResolver<Projection> {

    private final static Log logger = LogFactory.getLog(JpaProjectionResolver.class);

    @Override
    public Projection resolve(ProjectionQuery projectionQuery) {
        if(projectionQuery.getProjectionDefinition() != null) {
            switch (projectionQuery.getProjectionDefinition()) {
                case id:
                    return Projections.id();
                case property:
                    return Projections.property(projectionQuery.getPropertyName());
                case group:
                    return Projections.groupProperty(projectionQuery.getPropertyName());
                case distinct:
                    return Projections.distinct(Projections.property(projectionQuery.getPropertyName()));
                case count:
                    return Projections.count(projectionQuery.getPropertyName());
                case rowCount:
                    return Projections.rowCount();
                case countDistinct:
                    return Projections.countDistinct(projectionQuery.getPropertyName());
                case max:
                    return Projections.max(projectionQuery.getPropertyName());
                case min:
                    return Projections.min(projectionQuery.getPropertyName());
                case avg:
                    return Projections.avg(projectionQuery.getPropertyName());
                case sum:
                    return Projections.sum(projectionQuery.getPropertyName());
            }
        }
        logger.warn("ProjectionQuery wrong definition!");
        return null;
    }

}
